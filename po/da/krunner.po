# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Martin Schlander <mschlander@opensuse.org>, 2014, 2015, 2016, 2019.
# scootergrisen, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2019-11-22 20:19+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Martin Schlander"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mschlander@opensuse.org"

#: main.cpp:57 view.cpp:49
#, kde-format
msgid "KRunner"
msgstr "KRunner"

#: main.cpp:57
#, kde-format
msgid "Run Command interface"
msgstr "Brugerflade til at køre kommando"

#: main.cpp:63
#, kde-format
msgid "Use the clipboard contents as query for KRunner"
msgstr "Brug udklipsholderindholdet som forespørgsel til KRunner"

#: main.cpp:64
#, kde-format
msgid "Start KRunner in the background, don't show it."
msgstr "Start KRunner i baggrunden, vis den ikke."

#: main.cpp:65
#, kde-format
msgid "Replace an existing instance"
msgstr "Erstat en eksisterende instans"

#: main.cpp:66
#, kde-format
msgid "Show only results from the given plugin"
msgstr ""

#: main.cpp:67
#, kde-format
msgid "List available plugins"
msgstr ""

#: main.cpp:74
#, kde-format
msgid "The query to run, only used if -c is not provided"
msgstr "Den forespørgsel der skal køres, bruges kun hvis -c ikke er angivet"

#: main.cpp:83
#, kde-format
msgctxt "Header for command line output"
msgid "Available KRunner plugins, pluginId"
msgstr ""

#: qml/RunCommand.qml:100
#, kde-format
msgid "Configure"
msgstr ""

#: qml/RunCommand.qml:101
#, kde-format
msgid "Configure KRunner Behavior"
msgstr ""

#: qml/RunCommand.qml:104
#, kde-format
msgid "Configure KRunner…"
msgstr ""

#: qml/RunCommand.qml:117
#, kde-format
msgid "Showing only results from %1"
msgstr ""

#: qml/RunCommand.qml:131
#, kde-format
msgctxt "Textfield placeholder text, query specific KRunner plugin"
msgid "Search '%1'…"
msgstr ""

#: qml/RunCommand.qml:132
#, kde-format
msgctxt "Textfield placeholder text"
msgid "Search…"
msgstr ""

#: qml/RunCommand.qml:306 qml/RunCommand.qml:307 qml/RunCommand.qml:309
#, kde-format
msgid "Show Usage Help"
msgstr ""

#: qml/RunCommand.qml:317
#, kde-format
msgid "Pin"
msgstr ""

#: qml/RunCommand.qml:318
#, kde-format
msgid "Pin Search"
msgstr ""

#: qml/RunCommand.qml:320
#, kde-format
msgid "Keep Open"
msgstr ""

#: qml/RunCommand.qml:398 qml/RunCommand.qml:403
#, kde-format
msgid "Recent Queries"
msgstr ""

#: qml/RunCommand.qml:401
#, kde-format
msgid "Remove"
msgstr ""

#~ msgid "krunner"
#~ msgstr "krunner"

#, fuzzy
#~| msgid "KRunner"
#~ msgid "Show KRunner"
#~ msgstr "KRunner"

#, fuzzy
#~| msgid "KRunner"
#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "KRunner"
#~ msgstr "KRunner"

#~ msgid "Run Command on clipboard contents"
#~ msgstr "Kør kommando på udklipsholderindholdet"

#~ msgid "Run Command"
#~ msgstr "Kør kommando"

#~ msgctxt "Name for krunner shortcuts category"
#~ msgid "Run Command"
#~ msgstr "Kør kommando"
