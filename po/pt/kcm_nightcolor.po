# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kcm_nightcolor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2022-09-11 11:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: mín Roman Gilg máx HH Geo TUBS Wikimedia Commons BY\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: SA\n"

#: ui/DayNightView.qml:110
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"A temperatura da cor começa a mudar para a versão nocturna às %1 e muda por "
"completo às %2"

#: ui/DayNightView.qml:113
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"A temperatura da cor começa a mudar para a versão diurna às %1 e muda por "
"completo às %2"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Toque para escolher a sua localização no mapa."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Carregue para escolher a sua localização no mapa."

#: ui/LocationsFixedView.qml:79 ui/LocationsFixedView.qml:104
#, kde-format
msgid "Zoom in"
msgstr "Ampliar"

#: ui/LocationsFixedView.qml:210
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Modificado a partir do <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>Mapa de localizações do mundo</"
"link> pelo TUBS / Wikimedia Commons / <link url='https://creativecommons.org/"
"licenses/by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:223
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Latitude:"

#: ui/LocationsFixedView.qml:250
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Longitude:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr ""
"O filtro de luz azul torna as cores no ecrã mais quentes para reduzir o "
"cansaço ocular."

#: ui/main.qml:154
#, kde-format
msgid "Switching times:"
msgstr "Horas de mudança:"

#: ui/main.qml:157
#, kde-format
msgid "Always off"
msgstr "Sempre desligado"

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Pôr ou nascer do sol na localização actual"

#: ui/main.qml:159
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Pôr ou nascer do sol numa localização manual"

#: ui/main.qml:160
#, kde-format
msgid "Custom times"
msgstr "Horas personalizadas"

#: ui/main.qml:161
#, fuzzy, kde-format
#| msgid "Always on night color"
msgid "Always on night light"
msgstr "Sempre com a cor nocturna"

#: ui/main.qml:184
#, fuzzy, kde-format
#| msgid "Day color temperature:"
msgid "Day light temperature:"
msgstr "Temperatura da cor diurna:"

#: ui/main.qml:227 ui/main.qml:289
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1K"

#: ui/main.qml:232 ui/main.qml:294
#, fuzzy, kde-format
#| msgctxt "No blue light filter activated"
#| msgid "Cool (no filter)"
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Frio (sem filtro)"

#: ui/main.qml:239 ui/main.qml:301
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Quente"

#: ui/main.qml:246
#, fuzzy, kde-format
#| msgid "Night color temperature:"
msgid "Night light temperature:"
msgstr "Temperatura da cor nocturna:"

#: ui/main.qml:311
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr ""

#: ui/main.qml:317
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Latitude: %1°   Longitude: %2°"

#: ui/main.qml:339
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"A localização do dispositivo será actualizada periodicamente através de GPS "
"(se estiver disponível) ou enviando a informação da rede para os <a href="
"\"https://location.services.mozilla.com\">Serviços de Geo-Localização da "
"Mozilla</a>."

#: ui/main.qml:357
#, fuzzy, kde-format
#| msgid "Begin night color at:"
msgid "Begin night light at:"
msgstr "Iniciar a cor nocturna às:"

#: ui/main.qml:370 ui/main.qml:393
#, kde-format
msgid "Input format: HH:MM"
msgstr "Formato de entrada: HH:MM"

#: ui/main.qml:380
#, fuzzy, kde-format
#| msgid "Begin day color at:"
msgid "Begin day light at:"
msgstr "Iniciar a cor diurna às:"

#: ui/main.qml:402
#, kde-format
msgid "Transition duration:"
msgstr "Duração da transição:"

#: ui/main.qml:411
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minutos"

#: ui/main.qml:424
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Minutos de entrada - mín. 1, máx. 600"

#: ui/main.qml:442
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Erro: O tempo de transição sobrepõe-se."

#: ui/main.qml:465
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "A localizar…"

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Fria"

#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "Isto é como ficará a temperatura da cor diurna quando ficar activa."

#~ msgid "This is what night color temperature will look like when active."
#~ msgstr ""
#~ "Isto é como ficará a temperatura da cor nocturna quando ficar activa."
