# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023 Kisaragi Hiu <mail@kisaragi-hiu.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-12-24 06:50+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@lists.slat.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/ui/BrightnessItem.qml:71
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Brightness and Color"
msgstr "亮度與顏色"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Screen brightness at %1%"
msgstr "螢幕亮度為 %1%"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "鍵盤亮度為 %1%"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "夜光模式關閉"

#: package/contents/ui/main.qml:107
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "夜光模式色溫 %1K"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "捲動來調整螢幕亮度"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "按中鍵切換夜光模式"

#: package/contents/ui/main.qml:258
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "設定夜光…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "關閉"

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "無法使用"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "未啟用"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "並非執行中"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "開啟"

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "早晨過渡期間"

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "白天"

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "夜間過渡期間"

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "夜晚"

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "設定…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "啟用並設定…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "何時完成到白天的過渡："

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "何時開始到夜晚的過渡："

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "何時完成到夜晚的過渡："

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "何時開始到白天的過渡："

#: package/contents/ui/PopupDialog.qml:70
#, kde-format
msgid "Display Brightness"
msgstr "顯示亮度"

#: package/contents/ui/PopupDialog.qml:101
#, kde-format
msgid "Keyboard Brightness"
msgstr "鍵盤亮度"

#: package/contents/ui/PopupDialog.qml:132
#, kde-format
msgid "Night Light"
msgstr "夜光模式"
