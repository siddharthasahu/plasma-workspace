# Translation of plasmashell to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2015-04-28 14:02+0200\n"
"Last-Translator: Bjørn Steensrud <bjornst@skogkatt.homelinux.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Bjørn Steensrud"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "bjornst@skogkatt.homelinux.org"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr ""

#: desktopview.cpp:211
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "Plasma-skall"

#: desktopview.cpp:214
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "Plasma-skall"

#: desktopview.cpp:217
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "Plasma-skall"

#: desktopview.cpp:220
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "Plasma-skall"

#: desktopview.cpp:223
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "Plasma-skall"

#: desktopview.cpp:226
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "Plasma-skall"

#: desktopview.cpp:252
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "Plasma-skall"

#: desktopview.cpp:256
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "Plasma-skall"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr ""

#: desktopview.cpp:263
#, fuzzy, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "Plasma-skall"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr ""

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "Plasma-skall"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Slå på QML JavaScript feilsøker"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "Ikke start plasma-shell automatisk på nytt etter et krasj"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Tving innlasting av oppgitt shell-modul"

#: main.cpp:110
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: main.cpp:113
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""

#: main.cpp:114
#, kde-format
msgid "file"
msgstr "fil"

#: main.cpp:118
#, kde-format
msgid "Lists the available options for user feedback"
msgstr ""

#: main.cpp:200
#, kde-format
msgid "Plasma Failed To Start"
msgstr ""

#: main.cpp:201
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Dempet lyd"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr ""

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 dempet"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr ""

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr ""

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr ""

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr ""

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr ""

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr ""

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr ""

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr ""

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr ""

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr ""

#: panelview.cpp:999
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr ""

#: panelview.cpp:1001
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr ""

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr ""

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr ""

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr ""

#: shellcorona.cpp:198 shellcorona.cpp:200
#, kde-format
msgid "Show Desktop"
msgstr ""

#: shellcorona.cpp:200
#, kde-format
msgid "Hide Desktop"
msgstr ""

#: shellcorona.cpp:216
#, kde-format
msgid "Show Activity Switcher"
msgstr ""

#: shellcorona.cpp:227
#, kde-format
msgid "Stop Current Activity"
msgstr "Stopp nåværende aktivitet"

#: shellcorona.cpp:235
#, kde-format
msgid "Switch to Previous Activity"
msgstr ""

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Next Activity"
msgstr ""

#: shellcorona.cpp:258
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr ""

#: shellcorona.cpp:285
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr ""

#: shellcorona.cpp:307
#, kde-format
msgid "Move keyboard focus between panels"
msgstr ""

#: shellcorona.cpp:2052
#, kde-format
msgid "Add Panel"
msgstr "Legg til panel"

#: shellcorona.cpp:2094
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr ""

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr ""

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr ""

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr ""

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr ""

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr ""

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr ""
