# Translation of plasmashell.po to Catalan
# Copyright (C) 2014-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2016, 2020.
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2015, 2016, 2018, 2020, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-01-30 09:07+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Antoni Bella"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "antonibella5@yahoo.com"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "Configura el connector d'accions del ratolí"

#: desktopview.cpp:211
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "Plasma de KDE 6.0 Dev"

#: desktopview.cpp:214
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "Plasma de KDE 6.0 Alpha"

#: desktopview.cpp:217
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "Plasma de KDE 6.0 Beta 1"

#: desktopview.cpp:220
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "Plasma de KDE 6.0 Beta 2"

#: desktopview.cpp:223
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "Plasma de KDE 6.0 RC1"

#: desktopview.cpp:226
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "Plasma de KDE 6.0 RC2"

#: desktopview.cpp:252
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "Plasma de KDE %1 Dev"

#: desktopview.cpp:256
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "Plasma de KDE %1 beta"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr "Plasma de KDE %1 beta %2"

#: desktopview.cpp:263
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "Plasma de KDE %1"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr "Visiteu bugs.kde.org per a informar de problemes"

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "Intèrpret d'ordres del Plasma"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Habilita el depurador de JavaScript del QML"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr ""
"No reiniciar automàticament l'intèrpret d'ordres del Plasma després d'una "
"fallada"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Força la càrrega del connector d'intèrpret d'ordres indicat"

#: main.cpp:110
#, kde-format
msgid "Replace an existing instance"
msgstr "Substitueix una instància existent"

#: main.cpp:113
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"Activa el mode prova i especifica el fitxer en JavaScript de disposició per "
"a definir l'entorn de prova"

#: main.cpp:114
#, kde-format
msgid "file"
msgstr "fitxer"

#: main.cpp:118
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Llista les opcions disponibles per als comentaris dels usuaris"

#: main.cpp:200
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Ha fallat en iniciar el Plasma"

#: main.cpp:201
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"El Plasma és incapaç d'iniciar-se, atès que no ha pogut usar correctament "
"l'OpenGL 2 o el programari de reserva.\n"
"Si us plau, comproveu que els controladors gràfics estan configurats "
"correctament."

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Àudio silenciat"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "Micròfon silenciat"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 silenciat"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "Ratolí tàctil actiu"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "Ratolí tàctil inactiu"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wi-Fi activa"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wi-Fi inactiva"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth actiu"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth inactiu"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "Internet mòbil actiu"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "Internet mòbil inactiu"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "S'ha activat el teclat a la pantalla"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "S'ha desactivat el teclat a la pantalla"

#: panelview.cpp:999
#, kde-format
msgctxt "@action:inmenu"
msgid "Hide Panel Configuration"
msgstr "Oculta la configuració del plafó"

#: panelview.cpp:1001
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Panel Configuration"
msgstr "Mostra la configuració del plafó"

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "Pantalla interna a %1"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%1 %2 a %3"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "Pantalla desconnectada %1"

#: shellcorona.cpp:198 shellcorona.cpp:200
#, kde-format
msgid "Show Desktop"
msgstr "Mostra l'escriptori"

#: shellcorona.cpp:200
#, kde-format
msgid "Hide Desktop"
msgstr "Oculta l'escriptori"

#: shellcorona.cpp:216
#, kde-format
msgid "Show Activity Switcher"
msgstr "Mostra el commutador d'activitats"

#: shellcorona.cpp:227
#, kde-format
msgid "Stop Current Activity"
msgstr "Atura l'activitat actual"

#: shellcorona.cpp:235
#, kde-format
msgid "Switch to Previous Activity"
msgstr "Canvia a l'activitat anterior"

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Next Activity"
msgstr "Canvia a l'activitat següent"

#: shellcorona.cpp:258
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "Activa l'entrada %1 del gestor de tasques"

#: shellcorona.cpp:285
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "Gestiona escriptoris i plafons..."

#: shellcorona.cpp:307
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "Mou el focus del teclat entre els plafons"

#: shellcorona.cpp:2052
#, kde-format
msgid "Add Panel"
msgstr "Afegeix un plafó"

#: shellcorona.cpp:2094
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "%1 buit"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "Renderitzador per programari en ús"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "Renderitzador per programari en ús"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "La renderització pot estar degradada"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "No ho tornis a mostrar"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "Nombre de plafons"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "Compta els plafons"
